This is work in progress project
-
Project started: 10th July 2022. Project is continuously developed.

Project description:
-
Second project created for learning purpose. Goal of this project was to simulate To Do List. It enables user to create various categories of tasks and define tasks as well. It provides multiple endpoints that enable to perform operations on database which is connected to the application. As part of this project, I decided to focus also on Spring Security. It is partially implemented but not yet finished as project is in progress.
For now, user registration, password hashing, user login and JWT tokens generation is implemented.
Next goal is to: update OncePerRequestFilter and provide roles. Next aim, after finishing Spring Security, will be implementing logic for sending scheduled emails to the user with his tasks.

Technologies used:
-
- Java 17
- Spring
- Hibernate
- JUnit
- Mockito
- Maven
- JSON
- REST
- Swagger
- PostgreSql
- Postman (for testing endpoints)
- Jira (for defining tickets)