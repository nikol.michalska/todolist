package com.nikol.michalska;

import com.nikol.michalska.model.DTO.TaskDTO;
import com.nikol.michalska.model.entities.Category;
import com.nikol.michalska.model.entities.Task;
import com.nikol.michalska.model.enums.Status;
import com.nikol.michalska.model.mappers.TaskMapper;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TaskMapperTest {

    @Test
    public void mapTaskTest() {
        Task task = getTask();


        TaskMapper taskMapper = new TaskMapper();
        TaskDTO taskDTO = taskMapper.mapTask(task);

        assertEquals(task.getId(), taskDTO.getId());
        assertEquals(task.getStatus(), taskDTO.getStatus());
        assertEquals(task.getName(), taskDTO.getName());
        assertEquals(task.getCategory().getCategoryName(), taskDTO.getCategoryDTO().getCategoryName());


    }

    @Test
    public void mapTasksTest_if_no_tasks() {
        Task task = getTask();

        TaskMapper taskMapper = new TaskMapper();
        TaskDTO taskDTO = taskMapper.mapTask(task);

        assertEquals(task.getId(), taskDTO.getId());
        assertEquals(task.getStatus(), taskDTO.getStatus());
        assertEquals(task.getName(), taskDTO.getName());
        assertEquals(task.getCategory().getCategoryName(), taskDTO.getCategoryDTO().getCategoryName());

    }

    @Test
    public void mapsTasksTest_if_contains_tasks() {
        Task task = getTask();
        List<Task> taskList = new ArrayList<>();
        taskList.add(task);

        TaskMapper taskMapper = new TaskMapper();

        List<TaskDTO> taskDTOList = taskMapper.mapTasks(taskList);

        assertEquals(taskList.get(0).getId(), taskDTOList.get(0).getId());
        assertEquals((taskList.get(0).getName()), taskDTOList.get(0).getName());
        assertEquals(taskList.get(0).getCategory().getCategoryName(), taskDTOList.get(0).getCategoryDTO().getCategoryName());
        assertEquals(taskList.get(0).getStatus(), taskDTOList.get(0).getStatus());
    }

    private Task getTask() {
        Task task = new Task();

        Category category = new Category();
        category.setId(UUID.fromString("5c8b0023-6f08-4108-91d0-9dcef5220539"));
        category.setCategoryName("School");


        task.setName("Homework");
        task.setStatus(Status.AWAITING);
        task.setId(UUID.fromString("a86640b1-e222-4fd8-9fa5-333c85db9757"));
        task.setCategory(category);
        return task;
    }
}
