package com.nikol.michalska;

import com.nikol.michalska.model.DTO.CategoryDTO;
import com.nikol.michalska.model.entities.Category;
import com.nikol.michalska.model.mappers.CategoryMapper;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CategoryMapperTest {

    @Test
    public void mapCategoryTest() {
        Category category = getCategory();

        CategoryMapper categoryMapper = new CategoryMapper();

        CategoryDTO categoryDTO = categoryMapper.mapCategory(category);

        assertEquals(category.getId(), category.getId());
        assertEquals(category.getCategoryName(), categoryDTO.getCategoryName());
    }

    @Test
    public void mapCategoriesTest() {
        Category category = getCategory();
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(category);

        CategoryMapper categoryMapper = new CategoryMapper();
        List<CategoryDTO> categoryDTOList = categoryMapper.mapCategories(categoryList);

        assertEquals(categoryList.get(0).getId(), categoryDTOList.get(0).getId());
        assertEquals(categoryList.get(0).getCategoryName(), categoryDTOList.get(0).getCategoryName());


    }

    public Category getCategory() {
        Category category = new Category();
        category.setId(UUID.fromString("2cba178f-7948-486e-af32-5cf0f80bbfc3"));
        category.setCategoryName("School");
        return category;
    }
}
