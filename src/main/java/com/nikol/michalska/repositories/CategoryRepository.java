package com.nikol.michalska.repositories;

import com.nikol.michalska.model.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

public interface CategoryRepository extends JpaRepository<Category, UUID> {


    default Category findByIdOrThrow(UUID id) {
        return findById(id).orElseThrow(() -> new EntityNotFoundException("There is no such category: " + id));
    }
}
