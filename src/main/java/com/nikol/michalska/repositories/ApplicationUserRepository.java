package com.nikol.michalska.repositories;

import com.nikol.michalska.model.entities.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, UUID> {

    boolean existsByMail(String mail);

    Optional<ApplicationUser> findByLogin(String login);
}
