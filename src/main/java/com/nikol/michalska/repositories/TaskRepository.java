package com.nikol.michalska.repositories;

import com.nikol.michalska.model.entities.Task;
import com.nikol.michalska.model.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TaskRepository extends JpaRepository<Task, UUID> {

    Optional<Task> findById(UUID id);
    List<Task> findAllByCategoryId(UUID category_id);

    List<Task> findAllByStatus(Status status);

    List<Task> findAllByDate(LocalDate date);

    List<Task> findAllByCategoryIdAndStatus(UUID category_id, Status status);

    Task findByStatus(Status status);

    Task deleteTaskByStatus(Status status);

    default Task findByIdOrThrow(UUID id) {
        return findById(id).orElseThrow(() -> new EntityNotFoundException("There is no such task: " + id));
    }
}
