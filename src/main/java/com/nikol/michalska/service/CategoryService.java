package com.nikol.michalska.service;

import com.nikol.michalska.model.DTO.CategoryDTO;
import com.nikol.michalska.model.DTO.NewCategoryDTO;
import com.nikol.michalska.model.entities.Category;
import com.nikol.michalska.model.entities.Task;
import com.nikol.michalska.model.mappers.CategoryMapper;
import com.nikol.michalska.repositories.CategoryRepository;
import com.nikol.michalska.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CategoryService {

    private CategoryRepository categoryRepository;

    private TaskRepository taskRepository;

    private CategoryMapper categoryMapper = new CategoryMapper();

    @Autowired
    public CategoryService(CategoryRepository categoryRepository,
                           TaskRepository taskRepository) {
        this.categoryRepository = categoryRepository;
        this.taskRepository = taskRepository;
    }

    public List<CategoryDTO> getAllCategories() {
        List<Category> categoryList = categoryRepository.findAll();
        List<CategoryDTO> categoryDTOList = categoryMapper.mapCategories(categoryList);
        return categoryDTOList;
    }

    public CategoryDTO createNewCategory(NewCategoryDTO newCategoryDTO) {
        Category newCategory = new Category();
        newCategory.setId(newCategoryDTO.getCategoryId());
        newCategory.setCategoryName(newCategoryDTO.getCategoryName());
        newCategory.setListOfTasks(new ArrayList<>());

        if (newCategoryDTO.getTaskListId() != null) {
            for (int a = 0; a < newCategoryDTO.getTaskListId().size(); a++) {
                UUID taskId = newCategoryDTO.getTaskListId().get(a);
                Task task = taskRepository.findByIdOrThrow(taskId);
                newCategory.getListOfTasks().add(task);
            }
        }

        Category savedCategory = categoryRepository.save(newCategory);
        CategoryDTO categoryDTO = categoryMapper.mapCategory(savedCategory);
        return categoryDTO;
    }

    public CategoryDTO updateCategory(NewCategoryDTO newCategoryDTO) {
        Category foundedCategory = categoryRepository.findByIdOrThrow(newCategoryDTO.getCategoryId());
        foundedCategory.setCategoryName(newCategoryDTO.getCategoryName());
        foundedCategory.setListOfTasks(new ArrayList<>());

        if (foundedCategory.getListOfTasks() != null) {
            for (int a = 0; a < newCategoryDTO.getTaskListId().size(); a++) {
                UUID taskId = newCategoryDTO.getTaskListId().get(a);
                Task task = taskRepository.findByIdOrThrow(taskId);
                foundedCategory.getListOfTasks().add(task);
            }
        }
        Category savedCategory = categoryRepository.save(foundedCategory);
        CategoryDTO categoryDTO = categoryMapper.mapCategory(savedCategory);
        return categoryDTO;
    }
}
