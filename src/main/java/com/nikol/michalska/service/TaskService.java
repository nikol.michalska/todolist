package com.nikol.michalska.service;

import com.nikol.michalska.model.DTO.NewTaskDTO;
import com.nikol.michalska.model.DTO.TaskDTO;
import com.nikol.michalska.model.DTO.UpdatedStatusTaskDTO;
import com.nikol.michalska.model.DTO.UpdatedTaskDTO;
import com.nikol.michalska.model.entities.Category;
import com.nikol.michalska.model.entities.Task;
import com.nikol.michalska.model.enums.Status;
import com.nikol.michalska.model.mappers.TaskMapper;
import com.nikol.michalska.repositories.CategoryRepository;
import com.nikol.michalska.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class TaskService {

    private TaskRepository taskRepository;

    private CategoryRepository categoryRepository;

    private TaskMapper taskMapper = new TaskMapper();

    @Autowired
    public TaskService(TaskRepository taskRepository,
                       CategoryRepository categoryRepository) {
        this.taskRepository = taskRepository;
        this.categoryRepository = categoryRepository;
    }

    public List<TaskDTO> getAllTasksDTO() {
        List<Task> listOfTasks = taskRepository.findAll();
        List<TaskDTO> taskDTOList = taskMapper.mapTasks(listOfTasks);
        return taskDTOList;
    }

    public List<TaskDTO> getAllTasksDTOByCategory(UUID categoryId) {
        List<Task> taskList = taskRepository.findAllByCategoryId(categoryId);
        List<TaskDTO> taskDTOList = taskMapper.mapTasks(taskList);
        return taskDTOList;
    }

    public List<TaskDTO> getAllTasksDTOByStatus(Status status) {
        List<Task> taskList = taskRepository.findAllByStatus(status);
        List<TaskDTO> taskDTOList = taskMapper.mapTasks(taskList);
        return taskDTOList;
    }

    public List<TaskDTO> getAllTasksDTOByDate(LocalDate date) {
        List<Task> taskList = taskRepository.findAllByDate(date);
        List<TaskDTO> taskDTOList = taskMapper.mapTasks(taskList);
        return taskDTOList;
    }

    public List<TaskDTO> getAllTasksDTOByCategoryAndStatus(UUID categoryId, Status status) {
        List<Task> taskList = taskRepository.findAllByCategoryIdAndStatus(categoryId, status);
        List<TaskDTO> taskDTOList = taskMapper.mapTasks(taskList);
        return taskDTOList;
    }

    public Integer getNumberOfTasksByCategory(UUID categoryId) {
        List<Task> listOfTasks = taskRepository.findAllByCategoryId(categoryId);
        int numberOfTasks = listOfTasks.size();
        return numberOfTasks;
    }

    public Integer getNumberOfTasksByStatus(Status status) {
        List<Task> taskList = taskRepository.findAllByStatus(status);
        int numberOfTasks = taskList.size();
        return numberOfTasks;
    }

    public TaskDTO addNewTask(NewTaskDTO newTaskDTO) {
        Task newTask = new Task();
        newTask.setName(newTaskDTO.getName());
        newTask.setDescription(newTaskDTO.getDescription());
        newTask.setStatus(newTaskDTO.getStatus());
        newTask.setDate(newTaskDTO.getDate());

        Category category = categoryRepository.findByIdOrThrow(newTaskDTO.getCategoryId());
        newTask.setCategory(category);

        Task savedTask = taskRepository.save(newTask);
        TaskDTO taskDTO = taskMapper.mapTask(savedTask);
        return taskDTO;
    }

    public void deleteTask(UUID taskId) {
        Task foundedTask = taskRepository.findByIdOrThrow(taskId);
        if (foundedTask.getCategory() != null) {
            Category category = foundedTask.getCategory();
            category.getListOfTasks().remove(foundedTask);
            categoryRepository.save(category);
        } else {
            taskRepository.deleteById(taskId);
        }
    }

    public void deleteTaskByStatus(Status status) {
        List<Task> list = taskRepository.findAllByStatus(status);
        for (int a = 0; a < list.size(); a++) {
            Task task = list.get(a);
            if (task != null) {
                Category category = list.get(a).getCategory();
                category.getListOfTasks().remove(task);
                categoryRepository.save(category);
            } else {
                taskRepository.deleteTaskByStatus(status);
            }

        }
    }

    public TaskDTO updateTask(UpdatedTaskDTO updatedTaskDTO) {
        Task updatedTask = taskRepository.findByIdOrThrow(updatedTaskDTO.getId());
        updatedTask.setName(updatedTaskDTO.getName());
        updatedTask.setDescription(updatedTaskDTO.getDescription());
        updatedTask.setDate(updatedTaskDTO.getDate());
        updatedTask.setStatus(updatedTaskDTO.getStatus());

        Category category = categoryRepository.findByIdOrThrow(updatedTaskDTO.getCategoryId());
        updatedTask.setCategory(category);
        Task savedTask = taskRepository.save(updatedTask);
        TaskDTO taskDTO = taskMapper.mapTask(savedTask);
        return taskDTO;
    }

    public TaskDTO updateTaskStatus(UpdatedStatusTaskDTO updatedStatusTaskDTO) {
        Task foundedTask = taskRepository.findByIdOrThrow(updatedStatusTaskDTO.getId());
        foundedTask.setStatus(updatedStatusTaskDTO.getStatus());
        Task savedTask = taskRepository.save(foundedTask);
        TaskDTO taskDTO = taskMapper.mapTask(savedTask);
        return taskDTO;
    }

    public TaskDTO updateTaskCategory(UUID taskId, UUID categoryId) {
        Task foundedTask = taskRepository.findByIdOrThrow(taskId);
        Category foundedCategory = categoryRepository.findByIdOrThrow(categoryId);
        foundedTask.setCategory(foundedCategory);
        Task savedTask = taskRepository.save(foundedTask);
        TaskDTO taskDTO = taskMapper.mapTask(savedTask);
        return taskDTO;
    }
}
