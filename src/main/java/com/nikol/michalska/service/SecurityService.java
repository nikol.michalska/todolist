package com.nikol.michalska.service;

import com.nikol.michalska.exceptions.UserExistsException;
import com.nikol.michalska.model.DTO.UserDTO;
import com.nikol.michalska.model.DTO.UserRegistrationDTO;
import com.nikol.michalska.model.entities.ApplicationUser;
import com.nikol.michalska.model.mappers.ApplicationUserMapper;
import com.nikol.michalska.repositories.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SecurityService {

    private ApplicationUserRepository applicationUserRepository;

    private ApplicationUserMapper applicationUserMapper = new ApplicationUserMapper();

    private Argon2PasswordEncoder encoder = new Argon2PasswordEncoder();

    @Autowired
    public SecurityService(ApplicationUserRepository applicationUserRepository) {
        this.applicationUserRepository = applicationUserRepository;
    }

    public UserDTO createNewUser(UserRegistrationDTO userRegistrationDTO) {
        if (applicationUserRepository.existsByMail(userRegistrationDTO.getEmail())) {
            throw new UserExistsException("User with such email exists: " + userRegistrationDTO.getEmail());
        }
        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setLogin(userRegistrationDTO.getLogin());
        applicationUser.setMail(userRegistrationDTO.getEmail());
        applicationUser.setPassword(encoder.encode(userRegistrationDTO.getPassword()));
        ApplicationUser savedApplicationUser = applicationUserRepository.save(applicationUser);
        UserDTO userDTO = applicationUserMapper.mapUser(savedApplicationUser);
        return userDTO;
    }

    public boolean isPasswordCorrect(String plainPassword, String hashedPassword) {
        return encoder.matches(plainPassword, hashedPassword);
    }
}
