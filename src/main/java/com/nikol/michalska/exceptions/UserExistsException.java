package com.nikol.michalska.exceptions;


public class UserExistsException extends RuntimeException {

    public UserExistsException(String message) {
        super(message);
    }
}
