package com.nikol.michalska.controller;

import com.nikol.michalska.configuration.JwtTokenUtil;
import com.nikol.michalska.exceptions.BadCredentialsException;
import com.nikol.michalska.model.DTO.LoginDTO;
import com.nikol.michalska.model.DTO.UserDTO;
import com.nikol.michalska.model.DTO.UserRegistrationDTO;
import com.nikol.michalska.model.entities.ApplicationUser;
import com.nikol.michalska.model.mappers.ApplicationUserMapper;
import com.nikol.michalska.repositories.ApplicationUserRepository;
import com.nikol.michalska.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class SecurityController {

    private SecurityService securityService;

    private ApplicationUserRepository applicationUserRepository;

    private ApplicationUserMapper applicationUserMapper = new ApplicationUserMapper();

    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    public SecurityController(SecurityService securityService,
                              ApplicationUserRepository applicationUserRepository,
                              JwtTokenUtil jwtTokenUtil) {
        this.securityService = securityService;
        this.applicationUserRepository = applicationUserRepository;
        this.jwtTokenUtil = jwtTokenUtil;
    }


    @PostMapping(path = "api/security/create_user")
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserRegistrationDTO userRegistrationDTO) {
        UserDTO userDTO = securityService.createNewUser(userRegistrationDTO);
        return ResponseEntity.ok(userDTO);
    }

    @PostMapping(path = "api/security/login")
    public ResponseEntity<UserDTO> loginUser(@Valid @RequestBody LoginDTO loginDTO) {
        ApplicationUser applicationUser = applicationUserRepository.findByLogin(loginDTO.getLogin())
                .orElseThrow(() -> new BadCredentialsException("User does not exist"));
        boolean isPasswordCorrect = securityService.isPasswordCorrect(loginDTO.getPassword(), applicationUser.getPassword());
        if (!isPasswordCorrect) {
            throw new BadCredentialsException("Incorrect credentials");
        }
        UserDTO userDTO = applicationUserMapper.mapUser(applicationUser);

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, jwtTokenUtil.generateToken(userDTO.getLogin()));
        return ResponseEntity.ok()
                .headers(headers)
                .body(userDTO);

    }
}
