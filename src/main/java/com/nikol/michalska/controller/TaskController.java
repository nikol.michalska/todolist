package com.nikol.michalska.controller;

import com.nikol.michalska.model.DTO.NewTaskDTO;
import com.nikol.michalska.model.DTO.TaskDTO;
import com.nikol.michalska.model.DTO.UpdatedStatusTaskDTO;
import com.nikol.michalska.model.DTO.UpdatedTaskDTO;
import com.nikol.michalska.model.entities.Category;
import com.nikol.michalska.model.entities.Task;
import com.nikol.michalska.model.enums.Status;
import com.nikol.michalska.model.mappers.TaskMapper;
import com.nikol.michalska.repositories.CategoryRepository;
import com.nikol.michalska.repositories.TaskRepository;
import com.nikol.michalska.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RequestMapping("api/task/")
@RestController
public class TaskController {

    private TaskRepository taskRepository;

    private CategoryRepository categoryRepository;

    private TaskService taskService;

    private TaskMapper taskMapper = new TaskMapper();

    @Autowired
    public TaskController(TaskRepository taskRepository,
                          CategoryRepository categoryRepository,
                          TaskService taskService) {
        this.taskRepository = taskRepository;
        this.categoryRepository = categoryRepository;
        this.taskService = taskService;

    }

    @GetMapping(path = "get")
    public ResponseEntity<List<TaskDTO>> getAllTasksDTO() {
        List<TaskDTO> taskDTOList = taskService.getAllTasksDTO();
        return ResponseEntity.ok(taskDTOList);
    }

    @GetMapping(path = "get_by_category/{categoryId}")
    public ResponseEntity<List<TaskDTO>> getAllTasksDTOByCategory(@Valid @PathVariable @NotNull UUID categoryId) {
        List<TaskDTO> taskDTOList = taskService.getAllTasksDTOByCategory(categoryId);
        return ResponseEntity.ok(taskDTOList);
    }

    @GetMapping(path = "get_by_status/{status}")
    public ResponseEntity<List<TaskDTO>> getAllTasksByStatus(@Valid @PathVariable @NotNull Status status) {
        List<TaskDTO> taskDTOList = taskService.getAllTasksDTOByStatus(status);
        return ResponseEntity.ok(taskDTOList);
    }

    @GetMapping(path = "get_by_date/{date}")
    public ResponseEntity<List<TaskDTO>> getAllTasksByDate(@Valid @PathVariable @NotNull
                                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        List<TaskDTO> taskDTOList = taskService.getAllTasksDTOByDate(date);
        return ResponseEntity.ok(taskDTOList);
    }

    @GetMapping(path = "get_by_category_and_status/{categoryId}/{status}")
    public ResponseEntity<List<TaskDTO>> getAllTasksByCategoryAndStatus(@Valid @PathVariable @NotNull UUID categoryId,
                                                                        @Valid @PathVariable @NotNull Status status) {
        List<TaskDTO> taskDTOList = taskService.getAllTasksDTOByCategoryAndStatus(categoryId, status);
        return ResponseEntity.ok(taskDTOList);

    }

    @GetMapping(path = "count_all")
    public ResponseEntity<Long> countAllTasks() {
        Long numberOfAllTasks = taskRepository.count();
        return ResponseEntity.ok(numberOfAllTasks);
    }

    @GetMapping(path = "count_by_category/{categoryId}")
    public ResponseEntity<Integer> countTasksByCategory(@Valid @PathVariable @NotNull UUID categoryId) {
        int numberOfTasks = taskService.getNumberOfTasksByCategory(categoryId);
        return ResponseEntity.ok(numberOfTasks);
    }

    @GetMapping(path = "count_by_status/{status}")
    public ResponseEntity<Integer> countTasksByStatus(@Valid @PathVariable @NotNull Status status) {
        int numberOfTasks = taskService.getNumberOfTasksByStatus(status);
        return ResponseEntity.ok(numberOfTasks);
    }

    @PostMapping(path = "add_new")
    public ResponseEntity<TaskDTO> addNewTask(@Valid @RequestBody NewTaskDTO newTaskDTO) {
        TaskDTO taskDTO = taskService.addNewTask(newTaskDTO);
        return ResponseEntity.ok(taskDTO);
    }

    @DeleteMapping(path = "delete/{taskId}")
    public void deleteTask(@PathVariable UUID taskId) {
        taskService.deleteTask(taskId);

    }

    @DeleteMapping(path = "delete_by_status/{status}")
    public void deleteTaskByStatus(@PathVariable Status status) {
        taskService.deleteTaskByStatus(status);
    }


    @PutMapping(path = "update_task")
    public ResponseEntity<TaskDTO> updateTask(@Valid @RequestBody UpdatedTaskDTO updatedTaskDTO) {
        TaskDTO taskDTO = taskService.updateTask(updatedTaskDTO);
        return ResponseEntity.ok(taskDTO);
    }

    @PutMapping(path = "update_status")
    public ResponseEntity<TaskDTO> updateTaskStatus(@Valid @RequestBody UpdatedStatusTaskDTO updatedStatusTaskDTO) {
        TaskDTO taskDTO = taskService.updateTaskStatus(updatedStatusTaskDTO);
        return ResponseEntity.ok(taskDTO);
    }

    @PutMapping(path = "update_category/{taskId}/{categoryId}")
    public ResponseEntity<TaskDTO> updateTaskCategory(@Valid @NotNull @PathVariable UUID taskId,
                                                      @Valid @NotNull @PathVariable UUID categoryId) {
        TaskDTO taskDTO = taskService.updateTaskCategory(taskId, categoryId);
        return ResponseEntity.ok(taskDTO);

    }

}



