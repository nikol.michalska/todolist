package com.nikol.michalska.controller;

import com.nikol.michalska.model.DTO.CategoryDTO;
import com.nikol.michalska.model.DTO.NewCategoryDTO;
import com.nikol.michalska.model.mappers.CategoryMapper;
import com.nikol.michalska.repositories.CategoryRepository;
import com.nikol.michalska.repositories.TaskRepository;
import com.nikol.michalska.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("api/category/")
@RestController
public class CategoryController {

    private CategoryRepository categoryRepository;

    private TaskRepository taskRepository;

    private CategoryService categoryService;

    private CategoryMapper categoryMapper = new CategoryMapper();

    @Autowired
    public CategoryController(CategoryRepository categoryRepository,
                              CategoryService categoryService,
                              TaskRepository taskRepository) {
        this.categoryRepository = categoryRepository;
        this.categoryService = categoryService;
        this.taskRepository = taskRepository;
    }


    @GetMapping(path = "get_all")
    public ResponseEntity<List<CategoryDTO>> getAllCategories() {
        List<CategoryDTO> categoryDTOList = categoryService.getAllCategories();
        return ResponseEntity.ok(categoryDTOList);
    }

    @PostMapping(path = "create_new_category")
    public ResponseEntity<CategoryDTO> createNewCategory(@Valid @RequestBody NewCategoryDTO newCategoryDTO) {
        CategoryDTO categoryDTO = categoryService.createNewCategory(newCategoryDTO);
        return ResponseEntity.ok(categoryDTO);
    }

    @PutMapping(path = "update")
    public ResponseEntity<CategoryDTO> updateCategory(@Valid @RequestBody NewCategoryDTO newCategoryDTO) {
        CategoryDTO categoryDTO = categoryService.updateCategory(newCategoryDTO);
        return ResponseEntity.ok(categoryDTO);
    }
}
