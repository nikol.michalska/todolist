package com.nikol.michalska.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private ToDoApplicationFilter toDoApplicationFilter;

    @Autowired
    public SpringSecurityConfiguration(ToDoApplicationFilter toDoApplicationFilter) {
        this.toDoApplicationFilter = toDoApplicationFilter;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .addFilterBefore(toDoApplicationFilter, UsernamePasswordAuthenticationFilter.class).authorizeHttpRequests()
                .antMatchers("api/security/create_user").permitAll()
                .anyRequest().permitAll();
    }
}
