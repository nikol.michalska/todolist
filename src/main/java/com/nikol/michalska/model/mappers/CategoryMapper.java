package com.nikol.michalska.model.mappers;

import com.nikol.michalska.model.DTO.CategoryDTO;
import com.nikol.michalska.model.entities.Category;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.ArrayList;
import java.util.List;

public class CategoryMapper {


    public CategoryDTO mapCategory(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setCategoryName(category.getCategoryName());
        return categoryDTO;
    }

    public List<CategoryDTO> mapCategories(List<Category> categoryList) {
        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        if (categoryList != null) {
            for (int a = 0; a < categoryList.size(); a++) {
                CategoryDTO categoryDTO = mapCategory(categoryList.get(a));
                categoryDTOList.add(categoryDTO);
            }
        }

        return categoryDTOList;
    }
}
