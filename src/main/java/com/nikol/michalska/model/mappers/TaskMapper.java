package com.nikol.michalska.model.mappers;

import com.nikol.michalska.model.DTO.CategoryDTO;
import com.nikol.michalska.model.DTO.TaskDTO;
import com.nikol.michalska.model.entities.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskMapper {


    public TaskDTO mapTask(Task task) {
        CategoryMapper categoryMapper = new CategoryMapper();

        CategoryDTO categoryDTO = categoryMapper.mapCategory(task.getCategory());

        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setCategoryDTO(categoryDTO);
        taskDTO.setDate(task.getDate());
        taskDTO.setStatus(task.getStatus());

        return taskDTO;
    }

    public List<TaskDTO> mapTasks(List<Task> listOfTasks) {
        List<TaskDTO> taskDTOList = new ArrayList<>();
        if (listOfTasks != null) {
            for (int a = 0; a < listOfTasks.size(); a++) {
                TaskDTO taskDTO = mapTask(listOfTasks.get(a));
                taskDTOList.add(taskDTO);
            }
        }

        return taskDTOList;
    }
}
