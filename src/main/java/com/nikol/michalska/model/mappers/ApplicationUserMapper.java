package com.nikol.michalska.model.mappers;

import com.nikol.michalska.model.DTO.UserDTO;
import com.nikol.michalska.model.entities.ApplicationUser;

public class ApplicationUserMapper {

    public UserDTO mapUser(ApplicationUser applicationUser) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(applicationUser.getId());
        userDTO.setLogin(applicationUser.getLogin());
        userDTO.setMail(applicationUser.getMail());
        return userDTO;
    }
}
