package com.nikol.michalska.model.entities;

import com.nikol.michalska.model.enums.Status;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Setter
@Getter
public class Task {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "pg-uuid")
    private UUID id;

    private String name;

    private String description;

    private LocalDate date;

    @Enumerated (EnumType.ORDINAL)
    private Status status;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Category category;


}
