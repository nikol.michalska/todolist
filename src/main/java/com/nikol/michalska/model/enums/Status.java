package com.nikol.michalska.model.enums;

public enum Status {
    AWAITING, IN_PROGRESS, DONE
}
