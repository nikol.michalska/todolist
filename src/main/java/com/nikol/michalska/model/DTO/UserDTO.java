package com.nikol.michalska.model.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class UserDTO {

    private UUID id;

    private String login;

    private String mail;
}
