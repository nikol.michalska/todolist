package com.nikol.michalska.model.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter
@Getter
public class CategoryDTO {

    @NotNull
    @Valid
    private UUID id;

    @NotNull
    @Valid
    private String categoryName;


}
