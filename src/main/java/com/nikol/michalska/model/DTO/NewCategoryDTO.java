package com.nikol.michalska.model.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class NewCategoryDTO {

    @NotNull
    @Valid
    private UUID categoryId;

    @NotNull
    @Valid
    private String categoryName;

    private List<UUID> taskListId;

}
