package com.nikol.michalska.model.DTO;

import com.nikol.michalska.validations.ValidPassword;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Setter
@Getter
public class UserRegistrationDTO {

    @NotEmpty
    private String login;

    @ValidPassword
    private String password;

    @Email
    private String email;
}
