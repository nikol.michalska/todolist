package com.nikol.michalska.model.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Setter
@Getter
public class LoginDTO {

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;


}
