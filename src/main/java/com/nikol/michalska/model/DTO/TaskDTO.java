package com.nikol.michalska.model.DTO;

import com.nikol.michalska.model.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

@Setter
@Getter
public class TaskDTO {

    private UUID id;

   @NotEmpty
    private String name;

    @NotEmpty
    private String description;

   @NotEmpty
    private LocalDate date;

    @NotEmpty
    private Status status;

    @NotEmpty
    private CategoryDTO categoryDTO;
}
