package com.nikol.michalska.model.DTO;

import com.nikol.michalska.model.entities.Category;
import com.nikol.michalska.model.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

@Setter
@Getter
public class NewTaskDTO {

    @NotNull
    @Valid
    private String name;

    @NotNull
    @Valid
    private String description;

    @NotNull
    @Valid
    private LocalDate date;

    @NotNull
    @Valid
    private Status status;

    @NotNull
    @Valid
    private UUID categoryId;
}
