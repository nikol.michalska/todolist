package com.nikol.michalska.model.DTO;

import com.nikol.michalska.model.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter
@Getter
public class UpdatedStatusTaskDTO {

    private UUID id;

    @NotNull
    private Status status;
}
