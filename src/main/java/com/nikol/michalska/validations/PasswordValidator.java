package com.nikol.michalska.validations;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {


    @Override
    public boolean isValid(String password, ConstraintValidatorContext constraintValidatorContext) {

        if(StringUtils.isEmpty(password)){
            return false;
        }
        if(password.length() < 5){
            return false;
        }

        int upperCaseCount = 0;
        int digitCount = 0;

        for(char c : password.toCharArray()){
            if(Character.isDigit(c)){
                digitCount++;
            } else if(Character.isUpperCase(c)){
                upperCaseCount++;
            }
        }

        return upperCaseCount > 0 && digitCount > 0;
    }
}
