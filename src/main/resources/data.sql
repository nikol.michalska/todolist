INSERT INTO category(id, category_name)
VALUES ('fe46ad55-a581-414f-9512-94eb34718916', 'School');

INSERT INTO category(id, category_name)
VALUES ('a141d324-6dde-40cd-a0c8-d283eb3c081d', 'Shopping');

INSERT INTO task(id, name, description, date, status, category_id)
VALUES ('366e2124-0354-41b1-9223-945d7c2ecd61', 'Homework', 'Do french homework', '12-09-2022', 1, 'fe46ad55-a581-414f-9512-94eb34718916');

INSERT INTO task(id, name, description, date, status, category_id)
VALUES ('05c55fb3-f7a9-4cea-ba19-ba0e6132bb46', 'Buy diary', 'Buy milk and cheese', '11-09-2022', 0 , 'a141d324-6dde-40cd-a0c8-d283eb3c081d');

INSERT INTO category_list_of_tasks(category_id, list_of_tasks_id)
VALUES ('fe46ad55-a581-414f-9512-94eb34718916', '366e2124-0354-41b1-9223-945d7c2ecd61');

INSERT INTO category_list_of_tasks(category_id, list_of_tasks_id)
VALUES ('a141d324-6dde-40cd-a0c8-d283eb3c081d', '05c55fb3-f7a9-4cea-ba19-ba0e6132bb46');